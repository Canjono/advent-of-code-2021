class Day8

  def run
    puts 'day 8'

    solution1
    solution2
  end

  private

  def solution1
    entries = get_input
    amount_of_unique_number_segments = get_amount_of_unique_number_segments(entries)
    
    puts "solution1: #{amount_of_unique_number_segments}"
  end

  def solution2
    entries = get_input
    total = get_total_output_value(entries)

    puts "solution2: #{total}"
  end

  def get_input
    file = File.open('./day8/input.txt')
    content = file.readlines.map(&:chomp)
    file.close

    content.map do |line|
      splitted_line = line.split('|')
      Entry.new(splitted_line[0].split, splitted_line[1].split)
    end
  end

  def get_amount_of_unique_number_segments(entries)
    entries.inject(0) do |total, entry|
      total + entry.get_amount_of_unique_number_segments
    end
  end

  def get_total_output_value(entries)
    entries.inject(0) do |total, entry|
      total + entry.get_four_digit
    end
  end

end

class Entry

  def initialize(signal_patterns, output_values)
    @signal_patterns = signal_patterns
    @output_values = output_values
  end

  def get_amount_of_unique_number_segments
    @output_values.inject(0) do |amount, value|
      if value.length == 2 || value.length == 4 || value.length == 3 || value.length == 7
        amount + 1
      else
        amount
      end
    end
  end

  def get_four_digit
    one = @signal_patterns.find {|pattern| pattern.length == 2}
    @signal_patterns.delete_if {|pattern| pattern == one}
    four = @signal_patterns.find {|pattern| pattern.length == 4}
    @signal_patterns.delete_if {|pattern| pattern == four}
    seven = @signal_patterns.find {|pattern| pattern.length == 3}
    @signal_patterns.delete_if {|pattern| pattern == seven}
    eight = @signal_patterns.find {|pattern| pattern.length == 7}
    @signal_patterns.delete_if {|pattern| pattern == eight}
    nine = @signal_patterns.find {|pattern| pattern.length == 6 && contains?(pattern, four)}
    @signal_patterns.delete_if {|pattern| pattern == nine}
    zero = @signal_patterns.find {|pattern| pattern.length == 6 && contains?(pattern, one)}
    @signal_patterns.delete_if {|pattern| pattern == zero}
    six = @signal_patterns.find {|pattern| pattern.length == 6}
    @signal_patterns.delete_if {|pattern| pattern == six}
    three = @signal_patterns.find {|pattern| pattern.length == 5 && contains?(pattern, one)}
    @signal_patterns.delete_if {|pattern| pattern == three}
    five = @signal_patterns.find {|pattern| pattern.length == 5 && contains?(six, pattern)}
    @signal_patterns.delete_if {|pattern| pattern == five}
    two = @signal_patterns.last

    all_values = [
      Value.new(zero, 0),
      Value.new(one, 1),
      Value.new(two, 2),
      Value.new(three, 3),
      Value.new(four, 4),
      Value.new(five, 5),
      Value.new(six, 6),
      Value.new(seven, 7),
      Value.new(eight, 8),
      Value.new(nine, 9),
    ]

    first = all_values.find {|value| same?(value.segments, @output_values[0])}.num
    second = all_values.find {|value| same?(value.segments, @output_values[1])}.num
    third = all_values.find {|value| same?(value.segments, @output_values[2])}.num
    fourth = all_values.find {|value| same?(value.segments, @output_values[3])}.num

    (first + second + third + fourth).to_i
  end

  def contains?(container, item)
    contains_item = true
    item.split('').each do |letter|
      contains_item = false unless container.include?(letter)
    end
    contains_item
  end

  def same?(value1, value2)
    return false unless value1.length == value2.length

    is_same = true
    value1.split('').each do |letter|
      is_same = false unless value2.include?(letter)
    end
    is_same
  end

end

class Value
  attr_reader :segments, :num

  def initialize(segments, num)
    @segments = segments
    @num = num.to_s
  end
end