class Day5

  def run
    puts 'day 5'

    solution1
    solution2
  end

  private

  def solution1
    @include_diagonals = false
    input = get_input
    diagram = get_diagram(input)
    safe_points = get_safe_points(diagram)
    
    puts "solution1: #{safe_points}"
  end

  def solution2
    @include_diagonals = true
    input = get_input
    diagram = get_diagram(input)
    safe_points = get_safe_points(diagram)

    puts "solution2: #{safe_points}"
  end

  def get_input
    file = File.open('./day5/input.txt')
    content = file.readlines.map(&:chomp)
    file.close

    content.map do |line|
      start_and_end = line.split('->')
      start_xy = start_and_end.first.split(',')
      end_xy = start_and_end.last.split(',')

      {
        x1: start_xy[0].to_i,
        x2: end_xy[0].to_i,
        y1: start_xy[1].to_i,
        y2: end_xy[1].to_i
      }
    end
  end

  def get_diagram(coords)
    diagram = setup_empty_diagram(coords)
    populate_diagram(diagram, coords)
  end

  def setup_empty_diagram(coords)
    max_x = 0
    max_y = 0

    coords.each do |coord|
      max_x = coord[:x1] if coord[:x1] > max_x
      max_x = coord[:x2] if coord[:x2] > max_x
      max_y = coord[:y1] if coord[:y1] > max_y
      max_y = coord[:y2] if coord[:y2] > max_y
    end

    diagram = []

    for y_index in 0..max_y
      x_axis = []

      for x_index in 0..max_x
        x_axis.push(0)
      end

      diagram.push(x_axis)
    end

    diagram
  end

  def populate_diagram(diagram, coords)
    coords.each do |coord|
      if coord[:y1] == coord[:y2]
        add_horizontal_line(diagram, coord)
      elsif coord[:x1] == coord[:x2]
        add_vertical_line(diagram, coord)
      elsif @include_diagonals
        add_diagonal_line(diagram, coord)
      end
    end

    diagram
  end

  def add_horizontal_line(diagram, coord)
    start_x = coord[:x1] > coord[:x2] ? coord[:x2] : coord[:x1]
    end_x = coord[:x1] > coord[:x2] ? coord[:x1] : coord[:x2]

    for x_index in start_x..end_x
      diagram[coord[:y1]][x_index] += 1
    end
  end

  def add_vertical_line(diagram, coord)
    start_y = coord[:y1] > coord[:y2] ? coord[:y2] : coord[:y1]
    end_y = coord[:y1] > coord[:y2] ? coord[:y1] : coord[:y2]

    for y_index in start_y..end_y
      diagram[y_index][coord[:x1]] += 1
    end
  end

  def add_diagonal_line(diagram, coord)
    curr_x = coord[:x1]
    curr_y = coord[:y1]

    until curr_y == coord[:y2]
      diagram[curr_y][curr_x] += 1
      curr_y = curr_y > coord[:y2] ? curr_y - 1 : curr_y + 1
      curr_x = curr_x > coord[:x2] ? curr_x - 1 : curr_x + 1
    end

    diagram[curr_y][curr_x] += 1
  end

  def get_safe_points(diagram)
    count = 0

    diagram.each do |row|
      row.each do |coord|
        count += 1 if coord >= 2
      end
    end

    count
  end

end