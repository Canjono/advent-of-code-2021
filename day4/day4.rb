class Day4

  def run
    puts 'day 4'

    solution1
    solution2
  end

  private

  def solution1
    bingo = get_input
    bingo.play_to_win
    bingo_board = bingo.latest_winning_board
    result = bingo_board.sum_of_unmarked * bingo.winning_number

    puts "solution1: #{result}"
  end

  def solution2
    bingo = get_input
    bingo.play_to_lose
    bingo_board = bingo.latest_winning_board
    result = bingo_board.sum_of_unmarked * bingo.winning_number

    puts "solution2: #{result}"
  end

  def get_input
    file = File.open('./day4/input.txt')
    content = file.readlines.map(&:chomp)
    file.close

    drawn_numbers = content[0].split(',').map { |number| number.to_i }
    boards = create_boards(content.drop(2))

    Bingo.new(boards, drawn_numbers)
  end

  def create_boards(input_boards)
    boards = []
    index = 0

    until index >= input_boards.length
      board = Board.new(input_boards.slice(index, 5))
      boards.push(board)
      index += 6
    end

    boards
  end
  
end

class Bingo

  def initialize(boards, drawn_numbers)
    @boards = boards
    @drawn_numbers = drawn_numbers
    @latest_winning_number = nil
    @latest_winning_board = nil
  end

  def play_to_win
    @drawn_numbers.each do |drawn_number|
      mark_boards(drawn_number)
      bingo_board = check_bingo
      
      if bingo_board
        @latest_winning_number = drawn_number
        @latest_winning_board = bingo_board
        break
      end
    end
  end

  def play_to_lose
    @drawn_numbers.each do |drawn_number|
      mark_boards(drawn_number)
      bingo_board = check_bingo
      
      if bingo_board
        @latest_winning_number = drawn_number
        @latest_winning_board = bingo_board
      end
    end
  end

  def winning_number
    @latest_winning_number
  end

  def latest_winning_board
    @latest_winning_board
  end

  private
  
  def mark_boards(number)
    @boards.each { |board| board.mark(number) }
  end

  def check_bingo
    bingo_board = nil

    @boards.each do |board|
      bingo_board = board if board.bingo?
    end

    bingo_board
  end

end

class Board

  def initialize(rows)
    @already_got_bingo = false
    @rows = []

    rows.each do |row|
      cells = row.split.map { |number| Cell.new(number.to_i) }
      @rows.push(cells)
    end
  end

  def sum_of_unmarked
    sum = 0

    @rows.each do |row|
      row.each { |cell| sum += cell.number if !cell.marked? }
    end

    sum
  end

  def bingo?
    return false if @already_got_bingo

    @rows.each do |row|
      if row.all? { |cell| cell.marked? }
        @already_got_bingo = true
        break
      end
    end

    return true if @already_got_bingo

    column_index = 0

    until column_index == 5
      column = @rows.map { |row| row[column_index] }

      if column.all? { |cell| cell.marked? }
        @already_got_bingo = true
        return true
      end

      column_index += 1
    end
  end

  def mark(number)
    return if @already_got_bingo

    @rows.each do |row|
      row.each { |cell| cell.mark if cell.number == number }
    end
  end

  def print
    @rows.each do |row|
      row.each { |cell| puts cell.marked? }
    end
  end

end

class Cell

  def initialize(number)
    @number = number
    @marked = false
  end

  def number
    @number
  end

  def mark
    @marked = true
  end

  def marked?
    @marked
  end

end