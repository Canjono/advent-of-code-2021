class Day2

  def run
    puts "day 2"

    solution1
    solution2
  end

  private

  def solution1
    input = get_input
    destination = get_destination(input)
    result = multiply(destination[:horizontal], destination[:depth])

    puts "solution1: #{result}"
  end

  def solution2
    input = get_input
    destination = get_aimed_destination(input)
    result = multiply(destination[:horizontal], destination[:depth])

    puts "solution2: #{result}"
  end

  def get_input
    file = File.open("./day2/input.txt")
    lines = file.readlines.map(&:chomp)
    file.close

    lines.map do |line| 
      line_as_array = line.split
      {
        direction: line_as_array[0],
        steps: line_as_array[1].to_i
      }
    end
  end

  def get_destination(commands)
    position = {
      horizontal: 0,
      depth: 0
    }

    commands.each do |command|
      case command[:direction]
      when "forward" then position[:horizontal] += command[:steps]
      when "up" then position[:depth] -= command[:steps]
      when "down" then position[:depth] += command[:steps]
      end
    end

    position
  end

  def get_aimed_destination(commands)
    position = {
      horizontal: 0,
      depth: 0,
      aim: 0
    }

    commands.each do |command|
      case command[:direction]
      when "forward"
        position[:horizontal] += command[:steps]
        position[:depth] += position[:aim] * command[:steps]
      when "up" then position[:aim] -= command[:steps]
      when "down" then position[:aim] += command[:steps]
      end
    end

    position    
  end

  def multiply(num1, num2)
    num1 * num2
  end

end