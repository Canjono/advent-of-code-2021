require './day1/day1.rb'
require './day2/day2.rb'
require './day3/day3.rb'
require './day4/day4.rb'
require './day5/day5.rb'
require './day6/day6.rb'
require './day7/day7.rb'
require './day8/day8.rb'

day1 = Day1.new
day1.run

day2 = Day2.new
day2.run

day3 = Day3.new
day3.run

day4 = Day4.new
day4.run

day5 = Day5.new
day5.run

day6 = Day6.new
day6.run

day7 = Day7.new
day7.run

day8 = Day8.new
day8.run