# Day 7: The Treachery of Whales

class Day7

  def run
    puts 'day 7'

    solution1
    solution2
  end

  private

  def solution1
    @constant_fuel_rate = true
    @horizontal_positions = get_input
    least_fuel = get_least_fuel

    puts "solution1: #{least_fuel}"
  end

  def solution2
    @constant_fuel_rate = false
    @horizontal_positions = get_input
    least_fuel = get_least_fuel

    puts "solution2: #{least_fuel}"
  end

  def get_input
    file = File.open('./day7/input.txt')
    content = file.readlines.map(&:chomp)[0]
    file.close

    content.split(',').map {|position| position.to_i}
  end

  def get_least_fuel
    middle_position = get_middle_position
    current_goal_pos = middle_position
    @least_fuel = get_spent_fuel(current_goal_pos)

    go_rightwards(current_goal_pos)

    current_goal_pos = middle_position

    go_leftwards(current_goal_pos)

    @least_fuel
  end

  def get_middle_position
    leftmost_pos = @horizontal_positions.first
    rightmost_pos = @horizontal_positions.first

    @horizontal_positions.each do |position|
      if position < leftmost_pos
        leftmost_pos = position
      elsif position > rightmost_pos
        rightmost_pos = position
      end
    end

    (rightmost_pos - ((rightmost_pos - leftmost_pos) / 2)).floor
  end

  def go_rightwards(goal_pos)
    while true
      fuel = get_spent_fuel(goal_pos)

      break if fuel > @least_fuel

      least_fuel = fuel
      goal_pos += 1
    end
  end

  def go_leftwards(goal_pos)
    while true
      fuel = get_spent_fuel(goal_pos)

      break if fuel > @least_fuel

      @least_fuel = fuel
      goal_pos -= 1
    end
  end

  def get_spent_fuel(goal_pos)
    if @constant_fuel_rate
      @horizontal_positions.inject(0) do |sum, position|
        sum + (position - goal_pos).abs
      end
    else
      @horizontal_positions.inject(0) do |sum, position|
        steps = (position - goal_pos).abs
        fuel = (1..steps).inject(0) {|sum, step| sum + step}
        sum + fuel
      end
    end
  end

end
