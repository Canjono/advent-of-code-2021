class Day3

  def run
    puts "day 3"

    solution1
    solution2
  end

  private

  def solution1
    input = get_input
    rates = get_gamma_and_epsilon_rates(input)
    result = multiply(rates[:gamma].to_i(2), rates[:epsilon].to_i(2))

    puts "solution1 #{result}"
  end

  def solution2
    input = get_input
    rates = get_oxygen_and_ch2_rates(input)
    result = multiply(rates[:oxygen].to_i(2), rates[:ch2].to_i(2))

    puts "solution2 #{result}"
  end

  def get_input
    file = File.open("./day3/input.txt")
    lines = file.readlines.map(&:chomp)
    file.close

    lines
  end

  def get_gamma_and_epsilon_rates(binary_numbers)
    most_common_bits = []
    amount_of_bits = binary_numbers[0].length
    index = 0

    until index == amount_of_bits
      most_common = get_most_common_bit(binary_numbers, index)
      bit = most_common == 'zero' ? 0 : 1
      most_common_bits.push(bit)
      index += 1
    end

    gamma_rate = most_common_bits.join
    epsilon_rate = ''

    most_common_bits.each do |bit|
      epsilon_rate += bit == 0 ? '1' : '0'
    end

    {
      gamma: gamma_rate,
      epsilon: epsilon_rate
    }
  end

  def get_oxygen_and_ch2_rates(binary_numbers)
    amount_of_bits = binary_numbers[0].length
    index = 0
    filtered_numbers = binary_numbers

    until filtered_numbers.length == 1 || index == amount_of_bits
      most_common = get_most_common_bit(filtered_numbers, index)
      filtered_numbers = filtered_numbers.select do |number|
        most_common_bit = most_common == 'zero' ? 0 : 1
        number[index].to_i == most_common_bit
      end
      index += 1
    end

    oxygen_rate = filtered_numbers[0]
    ch2_rate = ''

    {
      oxygen: get_life_support_rate(binary_numbers, 'oxygen'),
      ch2: get_life_support_rate(binary_numbers, 'ch2')
    }
  end

  def get_life_support_rate(binary_numbers, type)
    amount_of_bits = binary_numbers[0].length
    index = 0
    filtered_numbers = binary_numbers

    until filtered_numbers.length == 1 || index == amount_of_bits
      most_common = get_most_common_bit(filtered_numbers, index)
      filtered_numbers = filtered_numbers.select do |number|
        filter_value = nil
        if type == 'oxygen'
          filter_value = most_common == 'zero' ? 0 : 1
        else
          filter_value = most_common == 'one' || most_common == 'same' ? 0 : 1
        end
        number[index].to_i == filter_value
      end
      index += 1
    end

    filtered_numbers[0]
  end

  def get_most_common_bit(binary_numbers, index)
    zeroes = 0
    ones = 0

    binary_numbers.each do |number|
      if number[index] == '0'
        zeroes += 1
      else
        ones += 1
      end
    end

    if zeroes > ones
      'zero'
    elsif ones > zeroes
      'one'
    else
      'same'
    end
  end

  def multiply(num1, num2)
    num1 * num2
  end

end