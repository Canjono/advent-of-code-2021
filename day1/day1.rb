class Day1

  def run
    puts 'day 1'

    solution1
    solution2
  end

  private

  def solution1
    input = get_input
    increases = get_amount_of_depth_increases(input)

    puts "solution 1: #{increases}"
  end

  def solution2
    input = get_input
    increases = get_amount_of_window_increases(input)

    puts "solution 2: #{increases}"
  end

  def get_input
    file = File.open("./day1/input.txt")
    content = file.readlines.map(&:chomp)
    file.close

    content.map { |line| line.to_i }
  end

  def get_amount_of_depth_increases(depths)
    previous_depth = depths.first

    depths.inject(0) do |amount, depth|
      has_increased = depth > previous_depth
      previous_depth = depth
      has_increased ? amount + 1 : amount
    end
  end

  def get_amount_of_window_increases(depths)
    amount = 0
    previous_window_sum = depths.take(3).inject(:+)

    depths.each_with_index do |depth, index|
      break if index == depths.length - 2

      window_sum = depths.slice(index, 3).inject(:+)
      has_increased = window_sum > previous_window_sum
      previous_window_sum = window_sum

      amount += 1 if index > 0 && has_increased
    end

    amount
  end

end
