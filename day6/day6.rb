class Day6

  def run
    puts 'day 6'

    solution1
    solution2
  end

  private

  def solution1
    @fish_groups = get_input
    amount_of_fish = get_amount_of_fish_after_days(80)

    puts "solution1: #{amount_of_fish}"
  end

  def solution2
    @fish_groups = get_input
    amount_of_fish = get_amount_of_fish_after_days(256)

    puts "solution2: #{amount_of_fish}"
  end

  def get_input
    file = File.open('./day6/input.txt')
    lines = file.readlines.map(&:chomp)
    file.close

    fish_groups = []
    timers = lines.first.split(',')

    for i in 0..6
      fish_group = timers.select { |timer| timer.to_i == i }
      fish_groups << FishGroup.new(i, fish_group.length)
    end

    fish_groups
  end

  def get_amount_of_fish_after_days(days)
    @new_fish_groups = []

    for i in 1..days
      next_day_for_new_groups
      next_day_for_old_groups
      concat_matching_new_and_old_groups
    end

    get_amount
  end

  def next_day_for_new_groups
    @new_fish_groups.each do |new_group|
      new_group.next_day
    end
  end

  def next_day_for_old_groups
    @fish_groups.each do |group|
      group.next_day

      if group.new_fish > 0
        @new_fish_groups << FishGroup.new(8, group.new_fish)
      end
    end
  end

  def concat_matching_new_and_old_groups
    new_group_with_timer_6 = @new_fish_groups.find { |new_group| new_group.timer == 6 }

    if new_group_with_timer_6
      fish_group = @fish_groups.find { |group| group.timer == 6 }
      fish_group.add_fish(new_group_with_timer_6.amount)
      @new_fish_groups.delete_if { |new_group| new_group.timer == 6 }
    end
  end

  def get_amount
    amount = 0

    @fish_groups.each { |group| amount += group.amount }
    @new_fish_groups.each { |group| amount += group.amount }

    amount
  end

end

class FishGroup
  attr_reader :timer, :amount, :new_fish

  def initialize(timer, amount)
    @timer = timer
    @amount = amount
    @new_fish = 0
  end

  def next_day
    if @timer == 0
      @timer = 6
      @new_fish = @amount
    else
      @timer -= 1
      @new_fish = 0
    end
  end

  def add_fish(amount)
    @amount += amount
  end

end